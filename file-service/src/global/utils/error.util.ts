import {
  HttpException,
  InternalServerErrorException,
  PayloadTooLargeException,
} from '@nestjs/common';

export const handleError = (error: any) => {
  // if (error?.status >= 500 || !(error instanceof HttpException))
  //   console.error(error);
  if (error?.status >= 500 || !(error instanceof HttpException))
    console.log(error);
  if (error instanceof HttpException) throw error;
  if (error.status === 413) throw new PayloadTooLargeException(error.message);
  throw new InternalServerErrorException(error.message ?? error);
};
