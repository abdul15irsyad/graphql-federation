import { ExecutionContext, Injectable } from '@nestjs/common';
import { GqlContextType, GqlExecutionContext } from '@nestjs/graphql';
import { Scope } from '@sentry/core';
import { Handlers } from '@sentry/node';
import { SentryInterceptor } from '@travelerdev/nestjs-sentry';

@Injectable()
export class GraphqlInterceptor extends SentryInterceptor {
  protected captureException(
    context: ExecutionContext,
    scope: Scope,
    exception: unknown,
  ) {
    if (context.getType<GqlContextType>() === 'graphql') {
      this.captureGraphqlException(
        scope,
        GqlExecutionContext.create(context),
        exception,
      );
    } else {
      super.captureException(context, scope, exception);
    }
  }

  private captureGraphqlException(
    scope: Scope,
    gqlContext: GqlExecutionContext,
    exception: unknown,
  ): void {
    const info = gqlContext.getInfo();
    const context = gqlContext.getContext();

    scope.setExtra('graphql type', info.parentType.name);

    if (context.req) {
      // req within graphql context needs modification in
      const data = Handlers.parseRequest({}, context.req, {});
      const { query, variables } = JSON.parse(data.request.data);

      scope.setExtra('req', { ...data.request, data: undefined });
      scope.setExtra('graphql query', query);
      scope.setExtra('graphql variables', variables);

      if (data.extra) scope.setExtras(data.extra);
      if (data.user) scope.setUser(data.user);
    }

    this.client.instance().captureException(exception);
  }
}
