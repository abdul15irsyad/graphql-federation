import { HttpException, Module } from '@nestjs/common';
import { AppService } from './app.service';
import { GraphQLModule } from '@nestjs/graphql';
import {
  ApolloFederationDriver,
  ApolloFederationDriverConfig,
} from '@nestjs/apollo';
import { join } from 'path';
import { TypeOrmModule } from '@nestjs/typeorm';
import datasource from './database/database.datasource';
import { AcceptLanguageResolver, I18nModule, QueryResolver } from 'nestjs-i18n';
import { NODE_ENV, SENTRY_DSN } from './app.config';
import { RedisModule } from './redis/redis.module';
import { GraphQLFormattedError } from 'graphql';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { GraphqlInterceptor } from './global/graphql.interceptor';
import { AppResolver } from './app.resolver';
import { SentryModule } from '@travelerdev/nestjs-sentry';
import { FileModule } from './file/file.module';
import { MemoryStoredFile, NestjsFormDataModule } from 'nestjs-form-data';
import { ServeStaticModule } from '@nestjs/serve-static';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      ...datasource.options,
      autoLoadEntities: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      renderPath: '/',
    }),
    SentryModule.forRoot({
      dsn: SENTRY_DSN,
      debug: false,
      enabled: NODE_ENV !== 'local',
      // enabled: true,
      environment: NODE_ENV,
    }),
    I18nModule.forRoot({
      logging: NODE_ENV !== 'production',
      resolvers: [
        { use: QueryResolver, options: ['lang'] },
        AcceptLanguageResolver,
      ],
      fallbackLanguage: 'en',
      loaderOptions: {
        path: join(__dirname, '/global/i18n/'),
        watch: true,
      },
      typesOutputPath:
        NODE_ENV !== 'production'
          ? join(__dirname, '..', 'src/global/i18n/i18n.generated.ts')
          : undefined,
    }),
    NestjsFormDataModule.config({
      storage: MemoryStoredFile,
      isGlobal: true,
      // limits: {
      //   fileSize: 100_000,
      // },
    }),
    GraphQLModule.forRoot<ApolloFederationDriverConfig>({
      driver: ApolloFederationDriver,
      path: '/graphql',
      status400ForVariableCoercionErrors: true,
      autoSchemaFile: {
        federation: 2,
        path: join(process.cwd(), 'schema.gql'),
      },
      context: ({ req, res }) => ({ req, res }),
      formatError: (error: GraphQLFormattedError) => {
        if (error.extensions.status === 404) {
          error.extensions.code = 'NOT_FOUND_ERROR';
        }
        return error;
      },
    }),
    RedisModule,
    FileModule,
  ],
  providers: [
    AppService,
    AppResolver,
    {
      provide: APP_INTERCEPTOR,
      useFactory: () =>
        new GraphqlInterceptor({
          filters: [
            {
              type: HttpException,
              filter: (exception: HttpException) => 500 > exception.getStatus(), // Only report 500 errors
            },
          ],
        }),
    },
  ],
})
export class AppModule {}
