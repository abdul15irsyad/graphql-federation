import { Query, Resolver } from '@nestjs/graphql';
import { InternalServerErrorException } from '@nestjs/common';
import { AppService } from './app.service';
import { handleError } from './global/utils/error.util';
import { I18nContext, I18nService } from 'nestjs-i18n';
import { I18nTranslations } from './global/i18n/i18n.generated';

@Resolver()
export class AppResolver {
  constructor(
    private appService: AppService,
    private i18n: I18nService<I18nTranslations>,
  ) {}

  @Query(() => String, { name: 'fileError' })
  error() {
    try {
      throw new InternalServerErrorException(
        this.i18n.t('error.ERROR_TESTING', {
          lang: I18nContext.current().lang,
        }),
      );
    } catch (error) {
      handleError(error);
    }
  }
}
