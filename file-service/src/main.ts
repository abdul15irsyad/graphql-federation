import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { I18nValidationExceptionFilter } from './global/i18n-validation-exception.filter';
import { I18nValidationPipe } from 'nestjs-i18n';
import datasource from './database/database.datasource';
import { NODE_ENV, PORT } from './app.config';
import { Logger } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger('Main');
  app.useGlobalFilters(new I18nValidationExceptionFilter());
  app.useGlobalPipes(
    new I18nValidationPipe({
      whitelist: true,
      transform: true,
      stopAtFirstError: true,
    }),
  );
  await datasource.initialize();
  await app.listen(PORT, () =>
    logger.log(`Application running on port ${PORT}, environment ${NODE_ENV}`),
  );
}
bootstrap();
