import { join } from 'path';
import { DataSource } from 'typeorm';
import {
  HOST,
  PORT,
  USERNAME,
  PASSWORD,
  NAME,
  POOL_SIZE,
} from './database.config';

const datasource = new DataSource({
  type: 'postgres',
  host: HOST,
  port: PORT,
  username: USERNAME,
  password: PASSWORD,
  database: NAME,
  poolSize: POOL_SIZE,
  entities: [join(__dirname, '..', '**', '*.entity.{ts,js}')],
  migrations: [join(__dirname, 'migrations', '*')],
  migrationsTableName: 'migrations',
  synchronize: false,
  logging: false,
});

export default datasource;
