import { DataSource, DeepPartial } from 'typeorm';
import { Seeder } from '@jorgebodega/typeorm-seeding';
import { SeederEntity } from '../entities/seeder.entity';
import { File } from '../../file/file.entity';
import { NODE_ENV } from '../../app.config';
import { pathExistsSync, readdirSync, unlinkSync } from 'fs-extra';

export default class FileSeeder extends Seeder {
  public async run(datasource: DataSource): Promise<void> {
    // if seeder already executed
    if (
      await datasource
        .getRepository(SeederEntity)
        .findOneBy({ name: FileSeeder.name })
    )
      return;

    const files: DeepPartial<File>[] = [];
    if (NODE_ENV !== 'production') {
      if (NODE_ENV === 'local') {
        const fileExists = readdirSync('./public/uploads');
        for await (const file of fileExists) {
          if (
            pathExistsSync(`./public/uploads/${file}`) &&
            file !== '.gitkeep'
          ) {
            unlinkSync(`./public/uploads/${file}`);
          }
        }
      }

      files.push(
        {
          id: '9a3da060-85e7-4bdc-9e77-60c8c19e5ff2',
          path: 'dummy',
          fileName: `dummy-product (1).jpg`,
          mime: 'image/jpg',
          originalFileName: `dummy-product (1).jpg`,
        },
        {
          id: '3d6a3ff4-e089-4d86-8fc1-baedbf98f8bc',
          path: 'dummy',
          fileName: `dummy-product (2).jpg`,
          mime: 'image/jpg',
          originalFileName: `dummy-product (2).jpg`,
        },
        {
          id: 'bdbfafc6-3b06-46a5-8943-1197b37a3ead',
          path: 'dummy',
          fileName: `dummy-product (3).jpg`,
          mime: 'image/jpg',
          originalFileName: `dummy-product (3).jpg`,
        },
        {
          id: '7351014d-5d8b-4328-81b0-6dfe820ffa4c',
          path: 'dummy',
          fileName: `dummy-product (4).jpg`,
          mime: 'image/jpg',
          originalFileName: `dummy-product (4).jpg`,
        },
      );
    }
    await datasource.getRepository(File).save(files, { chunk: 30 });

    // add to seeders table
    await datasource
      .getRepository(SeederEntity)
      .save({ name: FileSeeder.name });
  }
}
