import { MigrationInterface, QueryRunner } from 'typeorm';

export class File1705295540999 implements MigrationInterface {
  name = 'File1705295540999';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "file" ("id" uuid NOT NULL, "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "updated_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "deleted_at" TIMESTAMP WITH TIME ZONE, "path" character varying NOT NULL, "file_name" character varying NOT NULL, "original_file_name" character varying NOT NULL, "mime" character varying NOT NULL, CONSTRAINT "PK_36b46d232307066b3a2c9ea3a1d" PRIMARY KEY ("id")); COMMENT ON COLUMN "file"."path" IS 'path folder di server'; COMMENT ON COLUMN "file"."file_name" IS 'file name di server'; COMMENT ON COLUMN "file"."original_file_name" IS 'nama file asli'; COMMENT ON COLUMN "file"."mime" IS 'tipe mime'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "file"`);
  }
}
