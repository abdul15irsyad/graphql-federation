import { Resolver, Query, Args, ResolveReference } from '@nestjs/graphql';
import { FileService } from './file.service';
import { File } from './file.entity';
import { handleError } from '../global/utils/error.util';
import { FindAllFileDto } from './dto/find-all-file.dto';
import { PaginatedFile } from './paginated-file.object-type';
import { setMeta } from '../global/utils/object.util';

@Resolver(() => File)
export class FileResolver {
  constructor(private readonly fileService: FileService) {}

  @Query(() => PaginatedFile, { name: 'files', nullable: true })
  async findAll(
    @Args('findAllFileInput', { type: () => FindAllFileDto, nullable: true })
    findAllDto: FindAllFileDto,
  ) {
    try {
      const files = await this.fileService.findWithPagination(findAllDto);
      return {
        meta: setMeta({ page: findAllDto?.page, ...files }),
        data: files.data,
      };
    } catch (error) {
      handleError(error);
    }
  }

  @ResolveReference()
  async resolveReference(reference: { __typename: string; id: string }) {
    return await this.fileService.findOneBy({ id: reference.id });
  }
}
