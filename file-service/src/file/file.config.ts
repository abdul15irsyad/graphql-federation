import dotenv from 'dotenv';
dotenv.config();

export const UPLOAD_PATH = process.env.UPLOAD_PATH ?? 'uploads';
// max upload file size in Byte
export const MAX_UPLOAD_FILE_SIZE = process.env.MAX_UPLOAD_FILE_SIZE
  ? +process.env.MAX_UPLOAD_FILE_SIZE
  : 1024 * 1024 * 20;
// max upload image size in Byte
export const MAX_UPLOAD_IMAGE_SIZE = process.env.MAX_UPLOAD_IMAGE_SIZE
  ? +process.env.MAX_UPLOAD_IMAGE_SIZE
  : 1024 * 1024 * 10;

export const MAX_UPLOAD_EXCEL_CSV_SIZE = process.env.MAX_UPLOAD_EXCEL_CSV_SIZE
  ? +process.env.MAX_UPLOAD_EXCEL_CSV_SIZE
  : 1024 * 1024 * 100;

export const IMAGE_MIMES = [
  'image/jpg',
  'image/jpeg',
  'image/png',
  'image/webp',
];

export const CSV_MIMES = ['text/csv'];

export const EXCEL_MIMES = [
  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
  // 'application/vnd.ms-excel',
];
