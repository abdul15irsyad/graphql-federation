import { Directive, Field, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '../global/base.entity';
import { AfterLoad, Column, Entity } from 'typeorm';

@ObjectType()
@Entity('file')
@Directive('@key(fields: "id")')
export class File extends BaseEntity {
  @Field(() => String, { description: 'path folder di server' })
  @Column('varchar', { comment: 'path folder di server' })
  path: string;

  @Field(() => String, { description: 'file name di server' })
  @Column('varchar', { name: 'file_name', comment: 'file name di server' })
  fileName: string;

  @Field(() => String, { nullable: true, description: 'url lengkap' })
  url: string;

  @Field(() => String, { description: 'nama file asli' })
  @Column('varchar', { name: 'original_file_name', comment: 'nama file asli' })
  originalFileName: string;

  @Field(() => String, { description: 'tipe mime' })
  @Column('varchar', { comment: 'tipe mime' })
  mime: string;

  @AfterLoad()
  setFile() {
    this.url = this.url ?? `/${this.path}/${encodeURIComponent(this.fileName)}`;
  }
}
