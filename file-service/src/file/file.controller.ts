import { Body, Controller, Post } from '@nestjs/common';
import { FileService } from './file.service';
import { handleError } from '../global/utils/error.util';
import { CreateFileDto } from './dto/create-file.dto';
import { I18nContext, I18nService } from 'nestjs-i18n';
import { FormDataRequest } from 'nestjs-form-data';
import { I18nTranslations } from '../global/i18n/i18n.generated';

@Controller('files')
export class FileController {
  constructor(
    private readonly fileService: FileService,
    private readonly i18n: I18nService<I18nTranslations>,
  ) {}

  @Post()
  @FormDataRequest()
  async create(@Body() createFileDto: CreateFileDto) {
    try {
      const file = await this.fileService.uploadAndCreate({
        fileDto: createFileDto.file,
      });
      return {
        message: this.i18n.t('common', {
          args: { property: 'FILE' },
          lang: I18nContext.current().lang,
        }),
        data: file,
      };
    } catch (error) {
      handleError(error);
    }
  }
}
