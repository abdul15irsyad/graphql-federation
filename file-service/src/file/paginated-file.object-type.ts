import { PaginatedResponse } from '../global/paginated-response.object-type';
import { File } from './file.entity';

export const PaginatedFile = PaginatedResponse(File);
export type PaginatedFile = InstanceType<typeof PaginatedFile>;
