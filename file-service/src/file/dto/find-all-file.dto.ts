import { InputType } from '@nestjs/graphql';
import { FindAllDto } from '../../global/find-all.dto';

@InputType()
export class FindAllFileDto extends FindAllDto {}
