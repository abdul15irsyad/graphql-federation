import { DataSource, DeepPartial } from 'typeorm';
import { Seeder } from '@jorgebodega/typeorm-seeding';
import { SeederEntity } from '../entities/seeder.entity';
import { Product } from '../../product/product.entity';
import { NODE_ENV } from '../../app.config';
import { v4 as uuidv4 } from 'uuid';
import { random } from '../../global/utils/array.util';

export default class ProductSeeder extends Seeder {
  public async run(datasource: DataSource): Promise<void> {
    // if seeder already executed
    if (
      await datasource
        .getRepository(SeederEntity)
        .findOneBy({ name: ProductSeeder.name })
    )
      return;

    const products: DeepPartial<Product>[] = [];
    if (NODE_ENV !== 'production') {
      const photoIds = [
        '9a3da060-85e7-4bdc-9e77-60c8c19e5ff2',
        '3d6a3ff4-e089-4d86-8fc1-baedbf98f8bc',
        'bdbfafc6-3b06-46a5-8943-1197b37a3ead',
        '7351014d-5d8b-4328-81b0-6dfe820ffa4c',
      ];
      for (let i = 1; i <= 10; i++) {
        products.push({
          name: `Product ${i}`,
          photoId: random(photoIds),
        });
      }
    }
    await datasource.getRepository(Product).save(
      products.map((product) => ({ ...product, id: uuidv4() })),
      { chunk: 30 },
    );

    // add to seeders table
    await datasource
      .getRepository(SeederEntity)
      .save({ name: ProductSeeder.name });
  }
}
