import { MigrationInterface, QueryRunner } from 'typeorm';

export class Product21705378972629 implements MigrationInterface {
  name = 'Product21705378972629';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "product" ADD "photo_id" uuid`);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "product" DROP COLUMN "photo_id"`);
  }
}
