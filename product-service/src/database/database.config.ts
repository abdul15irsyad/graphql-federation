import dotenv from 'dotenv';
dotenv.config();

export const HOST = process.env.DB_HOST ?? 'localhost';
export const PORT = process.env.DB_PORT ? +process.env.DB_PORT : 5432;
export const USERNAME = process.env.DB_USERNAME ?? 'postgres';
export const PASSWORD = process.env.DB_PASSWORD ?? 'dbpassword';
export const NAME = process.env.DB_NAME ?? 'nestjsgraphql';
export const POOL_SIZE = process.env.DB_POOL_SIZE
  ? +process.env.DB_POOL_SIZE
  : 10;
