import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Res,
  StreamableFile,
} from '@nestjs/common';
import { FormDataRequest } from 'nestjs-form-data';
import { PutObjectDto } from './dto/put-object-minio.dto';
import { Response } from 'express';
import { MinioClientService } from './minio-client.service';

@Controller('minio')
export class MinioClientController {
  constructor(private minioClientService: MinioClientService) {}

  @Get('get-object/:name')
  async getObject(
    @Param('name') name: string,
    @Res({ passthrough: true }) res: Response,
  ) {
    const { object, stat } = await this.minioClientService.getObject(name);
    res.set({
      'Content-Type': stat.metaData['content-type'],
      'Content-Disposition': `filename="${stat.metaData['original-name']}"`,
    });
    return new StreamableFile(object);
  }

  @Post('put-object')
  @FormDataRequest()
  async putObject(@Body() putObjectDto: PutObjectDto) {
    const { object, filename } = await this.minioClientService.putObject(
      putObjectDto.file,
    );
    return {
      message: 'success put object',
      data: { name: filename, ...object },
    };
  }
}
