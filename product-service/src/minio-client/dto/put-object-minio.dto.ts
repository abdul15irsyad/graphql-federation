import { IsNotEmpty } from 'class-validator';
import { MaxFileSize, IsFile, MemoryStoredFile } from 'nestjs-form-data';
import { i18nValidationMessage } from 'nestjs-i18n';
import { I18nTranslations } from '../../global/i18n/i18n.generated';

export class PutObjectDto {
  @MaxFileSize(1024 * 1024 * 5, {
    message: i18nValidationMessage<I18nTranslations>(
      'validation.MAX_FILE_SIZE',
      {
        property: 'FILE',
        maxFileSizeInMB: 5,
      },
    ),
  })
  @IsFile({
    message: i18nValidationMessage<I18nTranslations>('validation.IS_FILE', {
      property: 'FILE',
    }),
  })
  @IsNotEmpty({
    message: i18nValidationMessage<I18nTranslations>(
      'validation.IS_NOT_EMPTY',
      {
        property: 'FILE',
      },
    ),
  })
  file: MemoryStoredFile;
}
