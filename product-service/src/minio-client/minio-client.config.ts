import { isNotEmpty } from 'class-validator';

export const MINIO_ENDPOINT = process.env.MINIO_ENDPOINT ?? 'localhost';
export const MINIO_BUCKET = process.env.MINIO_BUCKET ?? 'bucket';
export const MINIO_PORT = process.env.MINIO_PORT
  ? +process.env.MINIO_PORT
  : 9000;
export const MINIO_USE_SSL = isNotEmpty(process.env.MINIO_USE_SSL)
  ? Boolean(JSON.parse(process.env.MINIO_USE_SSL))
  : false;
export const MINIO_ACCESS_KEY = process.env.MINIO_ACCESS_KEY ?? 'access_key';
export const MINIO_SECRET_KEY = process.env.MINIO_SECRET_KEY ?? 'secret_key';
