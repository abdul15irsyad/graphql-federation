import { Injectable } from '@nestjs/common';
import { MinioService } from 'nestjs-minio-client';
import { MINIO_BUCKET } from './minio-client.config';
import { MemoryStoredFile } from 'nestjs-form-data';
import * as crypto from 'crypto';

@Injectable()
export class MinioClientService {
  constructor(private readonly minioService: MinioService) {}

  async getObject(name: string) {
    const object = await this.minioService.client.getObject(MINIO_BUCKET, name);
    const stat = await this.minioService.client.statObject(MINIO_BUCKET, name);
    return { object, stat };
  }

  async putObject(file: MemoryStoredFile) {
    const hashedFilename = crypto
      .createHash('md5')
      .update(Date.now().toString())
      .digest('hex');
    const extension = file.originalName.substring(
      file.originalName.lastIndexOf('.'),
      file.originalName.length,
    );
    const filename = hashedFilename + extension;
    const object = await this.minioService.client.putObject(
      MINIO_BUCKET,
      filename,
      file.buffer,
      {
        'Content-Type': file.mimetype,
        'Original-Name': file.originalName,
      },
    );
    return { object, filename };
  }
}
