export const cacheRevive = (key: string, value: string) => {
  if (['createdAt', 'updatedAt', 'deletedAt'].includes(key))
    return value && new Date(value);
  return value;
};
