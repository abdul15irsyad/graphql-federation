import { Args, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { Product } from './product.entity';
import { PaginatedProduct } from './paginated-product.object-type';
import { FindAllDto } from '../global/find-all.dto';
import { useCache } from '../global/utils/cache.util';
import { ProductService } from './product.service';
import { RedisService } from '../redis/redis.service';
import { I18nContext, I18nService } from 'nestjs-i18n';
import { cleanNull, setMeta } from '../global/utils/object.util';
import { handleError } from '../global/utils/error.util';
import { NotFoundException, ParseUUIDPipe } from '@nestjs/common';
import { isEmpty } from 'class-validator';
import { File } from '../file/file.entity';

@Resolver(() => Product)
export class ProductResolver {
  constructor(
    private productService: ProductService,
    private redisService: RedisService,
    private i18n: I18nService,
  ) {}

  @Query(() => PaginatedProduct, { name: 'products' })
  async findAll(
    @Args('findAllProductInput', { type: () => FindAllDto, nullable: true })
    findAllDto?: FindAllDto,
  ) {
    try {
      const products = await useCache(
        `products:${JSON.stringify(cleanNull(findAllDto))}`,
        () => this.productService.findWithPagination(findAllDto),
      );
      return {
        meta: setMeta({ page: findAllDto.page, ...products }),
        data: products.data,
      };
    } catch (error) {
      handleError(error);
    }
  }

  @Query(() => Product, { name: 'product' })
  async findOne(@Args('id', { type: () => String }, ParseUUIDPipe) id: string) {
    try {
      const product = await useCache(`product:${id}`, () =>
        this.productService.findOneBy({ id }),
      );
      if (isEmpty(product))
        throw new NotFoundException(
          this.i18n.t('error.NOT_FOUND', {
            args: { property: 'PRODUCT' },
            lang: I18nContext.current().lang,
          }),
        );
      return product;
    } catch (error) {
      handleError(error);
    }
  }

  @ResolveField(() => File, { nullable: true })
  photo(@Parent() product: Product) {
    return { __typename: 'File', id: product.photoId };
  }
}
