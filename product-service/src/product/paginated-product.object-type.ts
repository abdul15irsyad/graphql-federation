import { PaginatedResponse } from '../global/paginated-response.object-type';
import { Product } from './product.entity';

export const PaginatedProduct = PaginatedResponse(Product);
export type PaginatedProduct = InstanceType<typeof PaginatedProduct>;
