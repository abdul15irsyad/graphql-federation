import { Field, ObjectType } from '@nestjs/graphql';
import { BaseEntity } from '../global/base.entity';
import { Column, Entity } from 'typeorm';
import { File } from '../file/file.entity';

@Entity('product')
@ObjectType()
export class Product extends BaseEntity {
  @Field(() => String)
  @Column('varchar')
  name: string;

  @Field(() => String, { nullable: true })
  @Column('uuid', { name: 'photo_id', nullable: true })
  photoId?: string;

  @Field(() => File, { nullable: true })
  photo?: File;
}
