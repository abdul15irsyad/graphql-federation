import { Injectable } from '@nestjs/common';
import { Product } from './product.entity';
import {
  FindOptionsRelations,
  FindOptionsWhere,
  ILike,
  Repository,
} from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { parseOrderBy } from '../global/utils/string.util';
import { BaseService } from '../global/base.service';
import { IFindAll } from '../global/find-all.interface';

@Injectable()
export class ProductService extends BaseService<Product> {
  protected relations: FindOptionsRelations<Product> = {};

  constructor(
    @InjectRepository(Product)
    private productRepo: Repository<Product>,
  ) {
    super(productRepo);
  }

  async findWithPagination({
    page,
    limit,
    search,
    orderBy,
    orderDir,
  }: IFindAll = {}) {
    page = page ?? 1;
    orderBy = orderBy ?? 'createdAt';
    orderDir = orderDir ?? 'desc';
    const filter: FindOptionsWhere<Product> = {};
    const findOptionsWhere:
      | FindOptionsWhere<Product>
      | FindOptionsWhere<Product>[] = search
      ? [{ name: ILike(`%${search}%`), ...filter }]
      : filter;
    const totalAllData = await this.productRepo.countBy(findOptionsWhere);
    const data = await this.productRepo.find({
      where: findOptionsWhere,
      take: limit,
      skip: limit ? (page - 1) * limit : undefined,
      order: parseOrderBy(orderBy, orderDir),
      relations: this.relations,
    });
    const totalPage = limit
      ? Math.ceil(totalAllData / limit)
      : data.length > 0
      ? 1
      : null;
    return {
      totalPage,
      totalAllData,
      data,
    };
  }
}
